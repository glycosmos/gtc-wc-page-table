import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';

class PageTable extends PolymerElement {
  static get template() {
    return html`
<style type="text/css">

</style>

<iron-ajax auto="" url="https://test.sparqlist.glycosmos.org/sparqlist/api/gtc_accession_list?offset={{_calculateOffset(page)}}&limit={{limit}}" handle-as="json" last-response="{{resultdata}}"></iron-ajax>

<!-- The array is set as the <vaadin-grid>'s "items" property -->
<vaadin-grid aria-label="Basic Binding Example" items="[[resultdata]]">

  <vaadin-grid-column width="60px" flex-grow="0">
    <template class="header">#</template>
    <template>[[index]]</template>
    <!-- If necessary, the footer could be set using <template class="footer"> -->
    <template class="footer">#</template>
  </vaadin-grid-column>

  <vaadin-grid-column>
    <template class="header">AccessionNumber</template>
    <template>[[item.AccessionNumber]]</template>
    <template class="footer">AccessionNumber</template>
  </vaadin-grid-column>

  <vaadin-grid-column>
    <template class="header">MassLabel</template>
    <template>[[item.MassLabel]]</template>
    <template class="footer">MassLabel</template>
  </vaadin-grid-column>

  <vaadin-grid-column width="8em">
    <template class="header">CreateDate</template>
    <template>
      <div style="white-space: normal">[[item.CreateDate]]</div>
    </template>
    <template class="footer">CreateDate</template>
  </vaadin-grid-column>
</vaadin-grid>
`;
  }
/**
  <iron-ajax auto="" url="https://test.sparqlist.glyconavi.org/api/GlycoSampleList_v2_page?offset={{_calculateOffset(page)}}&limit={{limit}}" handle-as="json" last-response="{{resultdata}}"></iron-ajax>
  <x-array-data-provider items="{{resultdata}}"></x-array-data-provider>
*/
  constructor() {
    super();
  }
  static get properties() {
    return {
      resultdata: {
        notify: true,
        type: Object,
        value: function() {
          return new Object();
        }
      },
      page: {
        type: Object,
        notify: true
      },
      pageItem: {
        type: Object,
        notify: true
      },
      limit: {
        type: String,
        notify: true
      },
      limitItem: {
        type: Object,
        notify: true
      },
      offset: {
        type: String,
        notify: true
      },
      offsetItem: {
        type: Object,
        notify: true
      },
    };
  }
  _calculateOffset(page) {
    console.log("page:" + page);
    console.log("offset: " + this.limit*page);
    return this.limit*page;
  }

  _handleAjaxPostResponse(e) {
    console.log(e);
  }
  _handleAjaxPostError(e) {
    console.log('error: ' + e);
  }
}

customElements.define('wc-page-table', PageTable);
