## Glytoucan Data Table

displays data retrieved from glytoucan.

development document : [google doc](https://docs.google.com/document/d/1z9SS6rLaFJeUc9Uf81UMOrLrXZ1RYOnmsyEzoPXPyIQ/edit)
### Paging

paging-compatible using {{ page }} and {{ limit }} data variables.

### Demo

Visit demo/index.html to see live examples.
